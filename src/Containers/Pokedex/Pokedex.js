import React , { Component } from 'react';
import Pokecard from '../Pokecard/Pokecard';

class Pokedex extends Component {

    render() {

        let pokedexs = [
            {id: 4, name: 'Charmander', type: 'fire', base_experience: 62},
            {id: 7, name: 'Squirtle', type: 'water', base_experience: 63},
            {id: 11, name: 'Metapod', type: 'bug', base_experience: 72},
            {id: 12, name: 'Butterfree', type: 'flying', base_experience: 178},
            {id: 25, name: 'Pikachu', type: 'electric', base_experience: 112},
            {id: 39, name: 'Jigglypuff', type: 'normal', base_experience: 95},
            {id: 94, name: 'Gengar', type: 'poison', base_experience: 225},
            {id: 133, name: 'Eevee', type: 'normal', base_experience: 65}
          ];

        const randomCard = () => {
            let newArr = [];
            
            for(let i = pokedexs.length; i > 0; i--){
                const posCard = Math.floor(Math.random() * i);
                newArr.push(pokedexs[posCard]);
                pokedexs = [...pokedexs.slice(0, posCard), ...pokedexs.slice(posCard + 1)];
            }

            return newArr;
        }

        const newBoard = randomCard();

        console.log(newBoard);

        const Hand1Value = newBoard[0].base_experience + newBoard[1].base_experience + newBoard[2].base_experience + newBoard[3].base_experience;
        const Hand2Value = newBoard[4].base_experience + newBoard[5].base_experience + newBoard[6].base_experience + newBoard[7].base_experience;


        return(
            <v>
                <v className="Pokedex-Line1">
                    <h2>{Hand1Value > Hand2Value ? <span style={{color: "green"}}>You Win</span> : <span style={{color: "red"}}>You Lose</span>}</h2>
                    <h3>{Hand1Value}</h3>
                    <Pokecard 
                        id={newBoard[0].id}
                        name={newBoard[0].name}
                        type={newBoard[0].type}
                        exp={newBoard[0].base_experience}
                    />
                    <Pokecard 
                        id={newBoard[1].id}
                        name={newBoard[1].name}
                        type={newBoard[1].type}
                        exp={newBoard[1].base_experience}
                    />
                    <Pokecard 
                        id={newBoard[2].id}
                        name={newBoard[2].name}
                        type={newBoard[2].type}
                        exp={newBoard[2].base_experience}
                    />
                    <Pokecard 
                        id={newBoard[3].id}
                        name={newBoard[3].name}
                        type={newBoard[3].type}
                        exp={newBoard[3].base_experience}
                    />
                </v>

                <div className="Pokedex-Line2">
                    <h2>{Hand1Value < Hand2Value ? <span style={{color: "green"}}>You Win</span> : <span style={{color: "red"}}>You Lose</span>}</h2>
                    <h3>{Hand2Value}</h3>
                    <Pokecard 
                        id={newBoard[4].id}
                        name={newBoard[4].name}
                        type={newBoard[4].type}
                        exp={newBoard[4].base_experience}
                    />
                    <Pokecard 
                        id={newBoard[5].id}
                        name={newBoard[5].name}
                        type={newBoard[5].type}
                        exp={newBoard[5].base_experience}
                    />
                    <Pokecard 
                        id={newBoard[6].id}
                        name={newBoard[6].name}
                        type={newBoard[6].type}
                        exp={newBoard[6].base_experience}
                    />
                    <Pokecard 
                        id={newBoard[7].id}
                        name={newBoard[7].name}
                        type={newBoard[7].type}
                        exp={newBoard[7].base_experience}
                    />
                </div>
                
                
            </v>

        );
    }
}

export default Pokedex;