import React, { Component } from 'react';
import "./Pokecard.css";

class Pokecard extends Component {

    render() {

        const urlImg = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${this.props.id}.png`

        return(
            <div className="Pokecard">
                <h3>{this.props.name}</h3>
                <img className="Pokecard-img" alt="" src={urlImg} />
                <div>Type: {this.props.type}</div>
                <div>EXP: {this.props.exp}</div>
            </div>
        )
    }
}

export default Pokecard;